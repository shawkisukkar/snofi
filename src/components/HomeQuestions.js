/** @jsx jsx */
import { jsx, Box, Heading, Text, Grid } from "theme-ui";
import ExternalLink from "./ExternalLink";

export default function HomeQuestions() {
  return (
    <Box sx={{ paddingTop: [40, null, 96], paddingBottom: [64, null, 128] }}>
      <Grid gap={[24, null, 32]}>
        <Heading>Our Publications</Heading>
        <Text sx={{ maxWidth: 500 }}>
        We published 5 breakthrough research until now in cryptography, distributed systems, and game theory.
        </Text>
        <ExternalLink
          href="https://discord.foundation.app/"
          sx={{ color: "#54BCFB" }}
        >
          Snowfi.Research ↗
        </ExternalLink>
      </Grid>
    </Box>
  );
}
