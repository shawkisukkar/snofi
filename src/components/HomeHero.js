/** @jsx jsx */
import { jsx, Grid, Box, Heading, Text, Button } from "theme-ui";

import Link from "@docusaurus/Link";
import useBaseUrl from "@docusaurus/useBaseUrl";

export default function HomeHero() {
  return (
    <Grid gap={[24, null, 40]}>
      <Heading variant="display">
        A Decentralized Social Protocol to Build a Better Social Media Platforms
      </Heading>
      <Grid gap={[32, null, 48]}>
        <Text sx={{ maxWidth: 500 }}>
          We're building an open-source, decentralized, censorship-free, permissionlessly programmable social media which has a built-in payments with an infrastructure that allows users to own their data.        </Text>
        <Box>
          <Link to={useBaseUrl("#")}>
            <Button sx={{ width: ["100%", null, "auto"] }}>Sign Up</Button>
          </Link>
        </Box>
      </Grid>
    </Grid>
  );
}
